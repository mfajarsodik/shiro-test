package com.hevadevelop.shirotest;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.support.annotation.NonNull;

import com.hevadevelop.shirotest.adapter.AppItems;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MyAsyncTaskAppList extends AsyncTaskLoader<ArrayList<AppItems>>{

    private ArrayList<AppItems> mData;
    private boolean mHasResult = false;

    public MyAsyncTaskAppList(@NonNull final Context context) {
        super(context);

        onContentChanged();
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged()) {
            forceLoad();
        } else if (mHasResult) {
            deliverResult(mData);
        }
    }

    @Override
    public void deliverResult(final ArrayList<AppItems> data) {
        mData = data;
        mHasResult = true;
        super.deliverResult(data);
    }

    @Override
    public ArrayList<AppItems> loadInBackground() {
        SyncHttpClient client = new SyncHttpClient();

        final ArrayList<AppItems> appItemses = new ArrayList<>();
        String url = "https://test.shirobyte.com/API/list.php";

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                setUseSynchronousMode(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("data");

                    for (int i = 0; i < list.length(); i++) {
                        JSONObject appList = list.getJSONObject(i);
                        AppItems appItems = new AppItems(appList);
                        appItemses.add(appItems);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
        return appItemses;
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mHasResult) {
            onReleaseResource(mData);
            mData = null;
            mHasResult = false;
        }
    }

    protected void onReleaseResource(ArrayList<AppItems> mData) {
    }
}
