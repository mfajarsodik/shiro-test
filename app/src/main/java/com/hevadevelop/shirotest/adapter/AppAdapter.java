package com.hevadevelop.shirotest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hevadevelop.shirotest.R;

import java.util.ArrayList;

public class AppAdapter extends BaseAdapter{

    private ArrayList<AppItems> mData = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;

    public AppAdapter(Context context) {
        this.context = context;
        inflater     = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<AppItems> appItems) {
        mData = appItems;
        notifyDataSetChanged();
    }

    public void addItem(final AppItems appItems) {
        mData.add(appItems);
        notifyDataSetChanged();
    }

    public void clearData() {
        mData.clear();
    }

    @Override
    public int getCount() {
        if (mData == null) return 0;
        return mData.size();
    }

    @Override
    public AppItems getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_row_app, null);
            holder.textViewName    = (TextView)view.findViewById(R.id.tv_app_title);
            holder.textViewVersion = (TextView)view.findViewById(R.id.tv_app_version);
            holder.imageViewApp    = (ImageView)view.findViewById(R.id.img_app);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }
        String url = "https://test.shirobyte.com/image/" + mData.get(i).getImage_app();
        holder.textViewName.setText(mData.get(i).getName());
        holder.textViewVersion.setText(mData.get(i).getVersion());
        Glide.with(view).load(url).into(holder.imageViewApp);
        return view;
    }

    private static class ViewHolder {
        TextView textViewName;
        TextView textViewVersion;
        ImageView imageViewApp;
    }
}
