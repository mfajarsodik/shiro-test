package com.hevadevelop.shirotest.adapter;

import org.json.JSONObject;

public class AppItems {

    private String image_app, name, description, version;

    public AppItems(JSONObject jsonObject) {
        try {
            String image_app   = jsonObject.getString("image");
            String name        = jsonObject.getString("name");
            String description = jsonObject.getString("description");
            String version     = jsonObject.getString("version");

            this.image_app     = image_app;
            this.name          = name;
            this.description   = description;
            this.version       = version;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getImage_app() {
        return image_app;
    }

    public void setImage_app(String image_app) {
        this.image_app = image_app;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
