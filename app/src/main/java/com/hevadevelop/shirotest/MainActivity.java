package com.hevadevelop.shirotest;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.hevadevelop.shirotest.ParcelData.AppsData;
import com.hevadevelop.shirotest.adapter.AppAdapter;
import com.hevadevelop.shirotest.adapter.AppItems;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<AppItems>> {

    AppAdapter appAdapter;

    @BindView(R.id.progress_main_layout)
    ProgressBar progressBarMain;
    @BindView(R.id.lv_apps)
    ListView listView;

    public static String EXTRAS_APPSID = "EXTRAS_APPSID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        progressBarMain.setVisibility(View.INVISIBLE);

        appAdapter = new AppAdapter(this);
        appAdapter.notifyDataSetChanged();

        listView.setAdapter(appAdapter);

        String parameter = "";

        Bundle bundle = new Bundle();
        bundle.putString(parameter, EXTRAS_APPSID);

        getLoaderManager().initLoader(0, bundle, this);
    }

    public void submit(View v) {
        View parentRow     = (View)v.getParent();
        listView           = (ListView) parentRow.getParent();
        final int position = listView.getPositionForView(parentRow);
        AppsData appsData  = new AppsData();
        appsData.setName(appAdapter.getItem(position).getName());
        appsData.setDescription(appAdapter.getItem(position).getDescription());
        appsData.setImage_app(appAdapter.getItem(position).getImage_app());
        appsData.setVersion(appAdapter.getItem(position).getVersion());
        Intent toDetailApps = new Intent(MainActivity.this, DetailAppsActivity.class);
        toDetailApps.putExtra(DetailAppsActivity.EXTRAS_APPSID, appsData);
        startActivity(toDetailApps);
    }

    @Override
    public Loader<ArrayList<AppItems>> onCreateLoader(int i, Bundle bundle) {
        progressBarMain.setVisibility(View.VISIBLE);
        return new MyAsyncTaskAppList(this);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<AppItems>> loader, ArrayList<AppItems> appItems) {
        progressBarMain.setVisibility(View.INVISIBLE);
        appAdapter.setData(appItems);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<AppItems>> loader) {
        appAdapter.setData(null);
    }
}
