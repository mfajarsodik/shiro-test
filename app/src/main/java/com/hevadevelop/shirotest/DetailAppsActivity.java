package com.hevadevelop.shirotest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hevadevelop.shirotest.ParcelData.AppsData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailAppsActivity extends AppCompatActivity {

    public static String EXTRAS_APPSID = "EXTRAS_APPSID";

    @BindView(R.id.tv_name_detail)
    TextView tvNameDetail;
    @BindView(R.id.tv_app_description)
    TextView tvAppDescription;
    @BindView(R.id.tv_app_version)
    TextView tvAppVersion;
    @BindView(R.id.img_app_detail)
    ImageView imgAppDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_apps);
        ButterKnife.bind(this);

        AppsData appsData     = getIntent().getParcelableExtra(EXTRAS_APPSID);

        String txtNama        = "Nama Aplikasi : " + appsData.getName();
        tvNameDetail.setText(txtNama);

        String txtDescription = "Deskripsi Aplikasi : " + appsData.getDescription();
        tvAppDescription.setText(txtDescription);

        String txtVersion     = "Versi Aplikasi : " + appsData.getVersion();
        tvAppVersion.setText(txtVersion);

        String url            = "http://test.shirobyte.com/image/" + appsData.getImage_app();
        Glide.with(this).load(url).into(imgAppDetail);
    }
}
