package com.hevadevelop.shirotest.ParcelData;

import android.os.Parcel;
import android.os.Parcelable;

public class AppsData implements Parcelable {

    private String image_app, name, description, version;

    public String getImage_app() {
        return image_app;
    }

    public void setImage_app(String image_app) {
        this.image_app = image_app;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.image_app);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.version);
    }

    public AppsData() {
    }

    protected AppsData(Parcel in) {
        this.image_app = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.version = in.readString();
    }

    public static final Parcelable.Creator<AppsData> CREATOR = new Parcelable.Creator<AppsData>() {
        @Override
        public AppsData createFromParcel(Parcel source) {
            return new AppsData(source);
        }

        @Override
        public AppsData[] newArray(int size) {
            return new AppsData[size];
        }
    };
}
